from django.contrib import admin
from .models import *


class SettingPosition(admin.ModelAdmin):
    list_display = [field.name for field in Position._meta.get_fields()]


class SettingSchedule(admin.ModelAdmin):
    list_display = [field.name for field in Schedule._meta.get_fields()]


class SettingPersons(admin.ModelAdmin):
    list_display = [field.name for field in Persons._meta.get_fields()]


class SettingEquipment(admin.ModelAdmin):
    list_display = [field.name for field in Equipment._meta.get_fields()]


class SettingAuthorization(admin.ModelAdmin):
    list_display = [field.name for field in Authorization._meta.get_fields()]


admin.site.register(Schedule, SettingSchedule)
admin.site.register(Persons, SettingPersons)
admin.site.register(Position, SettingPosition)
admin.site.register(Equipment, SettingEquipment)
admin.site.register(Authorization, SettingAuthorization)
