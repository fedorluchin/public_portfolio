import re

from django import forms
from django.forms import DateTimeField, DateTimeInput

from .models import *


class LoginUserForm(forms.Form):
    email = forms.CharField(label="Логин",
                            widget=forms.TextInput(attrs={'class': 'form-input'}),
                            max_length=65)
    password = forms.CharField(label="Пароль",
                               widget=forms.PasswordInput(attrs={'class': 'form-input'}),
                               max_length=65)


class RegisterUserForm(forms.Form):
    username = forms.CharField(label="Логин")
    password = forms.CharField(label="Пароль",
                               widget=forms.PasswordInput(),
                               max_length=65)
    password2 = forms.CharField(label="Повтор пароля",
                                widget=forms.PasswordInput(),
                                max_length=65)
    email = forms.CharField(label="Электронная почта",
                            widget=forms.TextInput(),
                            max_length=65)

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError("Пароли не совпадают!")
        return cd['password']


class ScheduleCDForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = '__all__'

    # def clean_date(self):
    #     cd = self.cleaned_data
    #     if re.fullmatch(r'\\d{4}-\\d{2}-\\d{2}', str(cd['date'])):
    #         return cd['date']
    #     else:
    #         raise forms.ValidationError("Дата не соответствует формату!")


class DateTimeRangeForm(forms.Form):
    start_datetime = DateTimeField(
        widget=DateTimeInput(format='%Y-%m-%d %H:%M:%S', attrs={'type': 'datetime-local'})
    )
    end_datetime = DateTimeField(
        widget=DateTimeInput(format='%Y-%m-%d %H:%M:%S', attrs={'type': 'datetime-local'})
    )
