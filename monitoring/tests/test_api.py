import requests
from django.test import TestCase
from config import *
from rest_framework.test import APITestCase, APIClient
from django.urls import reverse
from ..models import *

data_register = {"username": "TestApi1",
                 "password": "1234",
                 "email": "testapi1@mail.ru"
                 }

data_login = {
    "email": "testapi1@mail.ru",
    "password": "1234"
}


class MSAPITest(TestCase):
    def test_api_register(self):
        response = requests.post(f'{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_REGISTER}',
                                 json=data_register)
        data = response.json()
        access_token_register = data['access_token']
        # print(access_token_register)
        assert response.status_code == 200

    def test_api_login(self):
        response = requests.post(f'{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_LOGIN}',
                                 json=data_login)
        data = response.json()
        access_token_login = data['access_token']
        # print("Token for login: \n", access_token_login)
        assert response.status_code == 200


class EngineerMainAPITest(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.client.cookies[ACCESS_TOKEN_COOKIE_NAME] = requests.post(
            f'{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_LOGIN}',
            json=data_login).json()['access_token']
        # self.obj = Schedule.objects.create(id_person=15, id_equipment=15, date="2050-05-15")

    def test_add_schedule(self):
        url = reverse('engineer_page')
        data = {
            'id_person': 120,
            'id_equipment': 120,
            'date': '2025-01-01',
            'add': 'add'
        }
        response = self.client.post(url, data, format='multipart')
        sh_add_exists = Schedule.objects.filter(id_person=120).exists()
        self.assertTrue(sh_add_exists)
        # print(Schedule.objects.all().values())
        # self.assertEqual(Schedule.objects.count(), 1)
        # self.obj.refresh_from_db()

    def test_delete_schedule(self):
        Schedule.objects.create(id_person=222, id_equipment=222, date="2222-02-22")
        sh_del1_exists = Schedule.objects.filter(id_person=222).exists()
        self.assertTrue(sh_del1_exists)
        url = reverse('engineer_page')
        data = {
            'id_person': 222,
            'id_equipment': 222,
            'date': '2222-02-22',
            'delete': 'delete'
        }
        response = self.client.post(url, data, format='multipart')
        sh_del2_exists = Schedule.objects.filter(id_person=222).exists()
        self.assertFalse(sh_del2_exists)


class AnalyticMainAPITest(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.client.cookies[ACCESS_TOKEN_COOKIE_NAME] = requests.post(
            f'{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_LOGIN}',
            json=data_login).json()['access_token']
        ChartVoltage.objects.create(
            date_time="2050-05-15 18:00:00.930000 +00:00",
            voltage=1000,
        )
        ChartVoltage.objects.create(
            date_time="2050-05-15 22:00:00.930000 +00:00",
            voltage=1100,
        )
        ChartVoltage.objects.create(
            date_time="2050-05-16 11:00:00.930000 +00:00",
            voltage=800,
        )

    def test_download_excel_data(self):
        url = reverse('analytic_page')
        data = {
            'download_excel_voltage': 'download_excel_voltage',
            'start_datetime': '2050-04-01 00:00:00',
            'end_datetime': '2050-06-01 23:59:59',
        }
        response = self.client.post(url, data, format='multipart')
        # print(ChartVoltage.objects.all().values())
        # print(response)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/vnd.ms-excel')
        # self.assertEqual(response['Content-Disposition'], 'attachment; filename="Voltage_data.xlsx"')

