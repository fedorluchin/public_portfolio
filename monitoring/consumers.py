import json
import datetime
import requests

from random import randint
from asyncio import sleep
from urllib.parse import urlparse, parse_qs

from asgiref.sync import sync_to_async, async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer, WebsocketConsumer

from monitoring.models import *
from config import *
from pykafka import KafkaClient
from pykafka.common import OffsetType

dict_of_joined_users = {}


def get_username_from_access_cookie(ac_cookie):
    username = requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_JWT_PERMISSIONS}",
                             json={
                                 "token": f"{ac_cookie}"
                             }
                             ).json()["username"]
    return username


class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        username_left = get_username_from_access_cookie(self.scope['cookies']['access_token_cookie'])
        dict_of_joined_users[self.room_group_name].remove(username_left)

        print(dict_of_joined_users)

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'user_update',
                'online_users': dict_of_joined_users[self.room_group_name],
            }
        )

        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        text_data_json = json.loads(text_data)

        if 'message' in text_data_json:
            message = text_data_json['message']
            username = text_data_json['username']

            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': message,
                    'username': username
                }
            )

            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'user_update',
                    'online_users': dict_of_joined_users[self.room_group_name],
                }
            )

        if 'username_joined' in text_data_json:
            username_joined = text_data_json['username_joined']

            if self.room_group_name in dict_of_joined_users:
                if username_joined not in dict_of_joined_users.get(self.room_group_name, []):
                    dict_of_joined_users[self.room_group_name].append(username_joined)
            else:
                dict_of_joined_users[self.room_group_name] = [username_joined]

            print(dict_of_joined_users)

            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'user_update',
                    'online_users': dict_of_joined_users[self.room_group_name],
                }
            )

    def chat_message(self, event):
        message = event['message']
        username = event['username']

        self.send(text_data=json.dumps({
            'event': "Send",
            'message': message,
            'username': username
        }))

    def user_update(self, event):
        online_users = event['online_users']

        self.send(text_data=json.dumps({
            'event': "UserUpdate",
            'online_users': online_users
        }))


class GraphConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        await self.accept()

        consumer = await self.create_consumer()

        for message in consumer:
            if message is not None:
                message = json.loads(message.value.decode('utf-8'))
                await self.send(json.dumps(
                    {
                        'voltage': message['sensor_value'],
                        'dt_value': message['timestamp']
                    }
                ))
                await sleep(5)

    async def create_consumer(self):
        client = KafkaClient(hosts=f"{KAFKA_HOST}:{KAFKA_PORT}")
        topic = client.topics[KAFKA_TOPIC_NAME_1]
        consumer = topic.get_simple_consumer(
            consumer_group=None,
            auto_offset_reset=OffsetType.LATEST,
            reset_offset_on_start=True)
        return consumer


    # async def connect(self):
    #     await self.accept()
    #
    #     while True:
    #         current_datetime = datetime.datetime.now()
    #         formatted_datetime = current_datetime.strftime('%Y-%m-%d %H:%M:%S')
    #         await self.send(json.dumps(
    #             {
    #                 'voltage': randint(-10, 260),
    #                 'dt_value': formatted_datetime
    #             }
    #         ))
    #         await sleep(5)

        # while True:
        #     chart_voltage_data = await sync_to_async(ChartVoltage.objects.all().last)()
        #
        #     if chart_voltage_data:
        #         voltage = chart_voltage_data.voltage
        #         dt_value = chart_voltage_data.date_time
        #
        #         # Преобразование datetime в строку формата ISO, чтобы сериализовать datetime формат
        #         dt_value_str = dt_value.isoformat()
        #
        #         await self.send(json.dumps(
        #             {
        #                 'voltage': voltage,
        #                 'dt_value': dt_value_str
        #             }
        #         ))
        #
        #     await sleep(1)
