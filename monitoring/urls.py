from django.urls import path

from . import views

urlpatterns = [
    path('home/', views.index, name='home'),
    path('login/', views.login_user, name='login'),
    path('register/', views.register_user, name='register'),
    path('analytic/', views.AnalyticMain.as_view(), name='analytic_page'),
    path('engineer/', views.EngineerMain.as_view(), name='engineer_page'),
    path('logout/', views.logout, name='logout'),
    path('room/', views.room, name='room'),
    path('<str:room_name>/', views.chat, name='chat'),
]
