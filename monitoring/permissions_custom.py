import requests
from config import *
from rest_framework import permissions


def verify_jwt_permission(req, perm_name):
    verify = requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_VERIFY}",
                           headers={
                               'Authorization': f"Bearer {req.COOKIES[ACCESS_TOKEN_COOKIE_NAME]}"
                           }
                           ).json().values()
    if verify:
        check_perm = requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_JWT_PERMISSIONS}",
                                   json={
                                       "token": f"{req.COOKIES[ACCESS_TOKEN_COOKIE_NAME]}"
                                   }
                                   ).json()["position"] == perm_name
        return check_perm
    else:
        return False


class IsAnalytic(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return verify_jwt_permission(request, ANALYTIC_PERMISSION_NAME)


class IsEngeener(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return verify_jwt_permission(request, ENGEENER_PERMISSION_NAME)


class IsTester(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return verify_jwt_permission(request, TESTER_PERMISSION_NAME)

