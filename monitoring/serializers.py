from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import serializers

from .models import *

class ScheduleSerializer(serializers.Serializer):
    id_person = serializers.IntegerField()
    id_equipment = serializers.IntegerField()
    date = serializers.DateField()
