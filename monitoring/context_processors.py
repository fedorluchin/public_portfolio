import requests

from config import *


def get_username(request):
    try:
        username = requests.post(
            f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_JWT_PERMISSIONS}",
            json={
                "token": f"{request.COOKIES[ACCESS_TOKEN_COOKIE_NAME]}"
            }
        ).json()["username"]
        return {'username': username}
    except:
        return {'username': None}
