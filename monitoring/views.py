import json
import requests
import pandas as pd

from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from django.utils.safestring import mark_safe
from requests import Response
from rest_framework.exceptions import NotAuthenticated
from rest_framework.views import APIView
from twisted.web.error import RenderError

from .permissions_custom import *
from .forms import *
from .models import *
from .serializers import *
from config import *


class BaseMain(APIView):
    data = {}

    def handle_exception(self, exc):
        if isinstance(exc, NotAuthenticated):
            return render(self.request, 'monitoring/access_denied.html', status=403)
        return super().handle_exception(exc)

    def get(self, request):
        return render_with_data(request, self.template_name, self.data)


class AnalyticMain(BaseMain):
    permission_classes = [IsAnalytic | IsTester]
    template_name = 'monitoring/analytic_screen.html'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.form_excel_voltage = DateTimeRangeForm()
        self.data = {
            "form_voltage": self.form_excel_voltage,
        }

    def download_excel_data(self, start_datetime, end_datetime):
        # Выборка данных из модели-таблицы на основе диапазона дат
        data = ChartVoltage.objects.filter(date_time__range=(start_datetime, end_datetime)).values()

        # Создание DataFrame с явным указанием формата даты и времени
        df = pd.DataFrame(data)
        df['date_time'] = pd.to_datetime(df['date_time']).dt.strftime('%Y-%m-%d %H:%M:%S')

        # Сортировка данных по столбцу 'date_time' в порядке возрастания
        df = df.sort_values(by='date_time', ascending=True)

        # Создание файла Excel с отсортированными данными
        response = HttpResponse(content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="Voltage_data.xlsx"'
        df.to_excel(response, index=False)
        return response

    def get(self, request):
        return super().get(request)

    def post(self, request):
        if 'download_excel_voltage' in request.POST:
            form = DateTimeRangeForm(request.POST)
            if form.is_valid():
                start_datetime = form.cleaned_data['start_datetime']
                end_datetime = form.cleaned_data['end_datetime']

                response = self.download_excel_data(start_datetime, end_datetime)
                return response
            else:
                return redirect('analytic_page')


class EngineerMain(BaseMain):
    permission_classes = [IsEngeener | IsTester]
    template_name = 'monitoring/engineer_screen.html'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.form_schedule = ScheduleCDForm()
        self.schedule_data_json = json.dumps(ScheduleSerializer(Schedule.objects.all().order_by('date'),
                                                                many=True).data)
        self.data = {
            "form": self.form_schedule,
            "schedule": self.schedule_data_json,
        }

    def get(self, request):
        return super().get(request)

    def post(self, request):
        if 'add' in request.POST:
            form = ScheduleCDForm(request.POST)
            if form.is_valid():
                feed = Schedule(
                    id_person=form.cleaned_data['id_person'],
                    id_equipment=form.cleaned_data['id_equipment'],
                    date=form.cleaned_data['date'],
                )
                feed.save()
        elif 'delete' in request.POST:
            form = ScheduleCDForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                Schedule.objects.get(id_person=cd["id_person"],
                                     id_equipment=cd["id_equipment"],
                                     date=cd["date"]
                                     ).delete()
        return redirect('engineer_page')


def process_user_form(request, form_class, service_path, success_redirect, template, error_message):
    if request.method == "POST":
        form = form_class(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user_response = requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{service_path}",
                                          json=cd)
            # user_response with code 200 = True, else reading how False
            if user_response:
                if service_path == AUTH_SERVICE_PATH_LOGIN:
                    response = HttpResponseRedirect(reverse(success_redirect))
                    response.set_cookie(ACCESS_TOKEN_COOKIE_NAME, user_response.cookies.get(ACCESS_TOKEN_COOKIE_NAME))
                    response.set_cookie(REFRESH_TOKEN_COOKIE_NAME, user_response.cookies.get(REFRESH_TOKEN_COOKIE_NAME))
                    return response
                else:
                    return HttpResponseRedirect(reverse(success_redirect))
            else:
                messages.error(request, error_message)
    else:
        form = form_class()

    return render(request, template, {'form': form})


def login_user(request):
    form_class = LoginUserForm
    service_path = AUTH_SERVICE_PATH_LOGIN
    success_redirect = 'home'
    login_template = 'monitoring/login.html'
    error_message = 'Неправильный логин или пароль!'

    return process_user_form(request, form_class, service_path,
                             success_redirect, login_template,
                             error_message)


def register_user(request):
    form_class = RegisterUserForm
    service_path = AUTH_SERVICE_PATH_REGISTER
    success_redirect = 'home'
    register_template = 'monitoring/register.html'
    error_message = 'Невозможно зарегистрировать такого пользователя!'

    return process_user_form(request, form_class, service_path,
                             success_redirect, register_template,
                             error_message)


def render_with_data(req, render_page: str, json_data: dict = None):
    try:
        return render(req, render_page, json_data)
    except RenderError:
        return redirect('home')


def logout(request):
    response = HttpResponseRedirect(reverse('login'))
    response.delete_cookie(ACCESS_TOKEN_COOKIE_NAME)
    response.delete_cookie(REFRESH_TOKEN_COOKIE_NAME)
    return response


def room(request):
    return render_with_data(request, 'monitoring/choose_room.html')


def chat(request, room_name):
    room_name_json = mark_safe(json.dumps(room_name))
    return render_with_data(request, 'monitoring/chat.html',
                            {
                                'room_name_json': room_name_json,
                            })


def index(request):
    return render_with_data(request, 'monitoring/home.html')
