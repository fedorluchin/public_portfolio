import datetime
from django.db import models


# Расписание
class Schedule(models.Model):
    class Meta:
        db_table = 'Schedule'
        verbose_name = 'Расписание'
        verbose_name_plural = 'Расписание'

    id_person = models.IntegerField()
    id_equipment = models.IntegerField()
    date = models.DateField()


# Сотрудники
class Persons(models.Model):
    class Meta:
        db_table = 'Persons'
        verbose_name = 'Сотрудники'
        verbose_name_plural = 'Сотрудники'

    id_person = models.IntegerField()
    id_position = models.IntegerField()
    surname = models.CharField(max_length=200)
    name = models.CharField(max_length=200)


# Должности
class Position(models.Model):
    class Meta:
        db_table = 'Position'
        verbose_name = 'Должности'
        verbose_name_plural = 'Должности'

    id_person = models.IntegerField('ID персоны')
    position = models.CharField('Должность', max_length=200)


# Оборудование
class Equipment(models.Model):
    class Meta:
        db_table = 'Equipment'
        verbose_name = 'Оборудование'
        verbose_name_plural = 'Оборудование'

    id_equipment = models.IntegerField()
    title = models.CharField(max_length=200)
    characteristics = models.CharField(max_length=200)


# Авторизация
class Authorization(models.Model):
    class Meta:
        db_table = 'Authorization'
        verbose_name = 'Авторизация'
        verbose_name_plural = 'Авторизация'

    id_person = models.IntegerField()
    login = models.CharField(max_length=200)
    password = models.TextField(max_length=200)


# Показатели с датчика напряжения
class ChartVoltage(models.Model):
    class Meta:
        db_table = 'ChartVoltage'
        verbose_name = 'Вольтаж'
        verbose_name_plural = 'Вольтаж'

    date_time = models.DateTimeField()
    voltage = models.FloatField()

