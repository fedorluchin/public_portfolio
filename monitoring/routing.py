from django.urls import path, re_path
from .consumers import *

ws_urlpatterns = [
    path('ws/graph/', GraphConsumer.as_asgi()),
    re_path(r'ws/chat/(?P<room_name>\w+)/$', ChatConsumer.as_asgi()),
]
