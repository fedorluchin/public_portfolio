import os
import sys
import xlrd
import django

import Core_service.settings

os.environ['DJANGO_SETTINGS_MODULE'] = 'Core_service.settings'
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
django.setup()

from monitoring.models import *


class UploadingTables(object):
    model = Equipment
    foreign_key_fields = [""]

    def __init__(self, data):
        data = data
        self.uploaded_file = data.get("file")
        self.parsing()

    def getting_related_model(self, field_name):
        model = self.model
        related_model = model._meta.get_field(field_name).rel.to
        return related_model

    def getting_headers(self):
        s = self.s
        headers = dict()
        for column in range(s.ncols):
            value = s.cell(0, column).value
            headers[column] = value
        return headers

    def parsing(self):
        uploaded_file = self.uploaded_file
        wb = xlrd.open_workbook(file_contents=uploaded_file.read())
        s = wb.sheet_by_index(0)
        self.s = s

        headers = self.getting_headers()
        print(headers)

        data_bulk_list = list()
        for row in range(1, s.nrows):
            row_dict = {}
            for column in range(s.ncols):
                value = s.cell(row, column).value
                field_name = headers[column]

                if field_name == "id" and not value:
                    continue

                if field_name in self.foreign_key_fields:
                    related_model = self.getting_related_model(field_name)
                    print(related_model)

                    instance, created = related_model.objects.get_or_create(name=value)
                    value = instance

                row_dict[field_name] = value

            print(row_dict)
            # data_bulk_list.append(Equipment(**row_dict))
            Equipment.objects.create(**row_dict)

        Equipment.objects.bulk_create(data_bulk_list)
        return True
