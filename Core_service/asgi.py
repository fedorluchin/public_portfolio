"""
ASGI config for Core_service project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

# from public_portfolio.monitoring.routing import ws_urlpatterns
from monitoring.routing import ws_urlpatterns

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Core_service.settings')

application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(URLRouter(ws_urlpatterns)),
    'http': get_asgi_application(),
})