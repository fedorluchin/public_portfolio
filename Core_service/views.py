from django.contrib import messages
from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import redirect, render
from config import *
import requests
import json

from utils.uploadings import *
from .permissions_custom import *


def put_jwt_cookies():
    r = requests.post('http://127.0.0.1:5000/login', json={"email": "testflask@xx.xx", "password": "engin1337@!"})
    # print(r.cookies.items())
    print('Ответ на пост запрос к сервису авторизации:', r.json())
    list_cookies_response = r.cookies.get_dict()
    # print(list_cookies_response)
    response = Response()
    response.set_cookie(key='access_token_cookie', value=list_cookies_response['access_token_cookie'])
    response.set_cookie(key='refresh_token_cookie', value=list_cookies_response['refresh_token_cookie'])
    return response


class LoginJWTAuth(APIView):
    def post(self, request):
        creds_data_in = json.loads(json.dumps(request.data))
        print(creds_data_in)
        as_login_response = requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_LOGIN}",
                                          json=creds_data_in)
        list_cookies_response = as_login_response.cookies.get_dict()
        response = Response()
        response.set_cookie(key='access_token_cookie',
                            value=list_cookies_response['access_token_cookie'])
        response.set_cookie(key='refresh_token_cookie',
                            value=list_cookies_response['refresh_token_cookie'])
        response.data = as_login_response.json()
        return response

    def get(self, request):
        return Response({'INFO': 'GET METHOD DATA'})


class RegisterJWTAuth(APIView):
    def post(self, request):
        register_data_in = json.loads(json.dumps(request.data))
        as_register_response = requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_REGISTER}",
                                             json=register_data_in)
        response = Response()
        response.data = as_register_response.json()
        return response


class RefreshJWTAuth(APIView):
    def post(self, request):
        refresh_data_in = request.headers
        as_refresh_response = requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_REFRESH}",
                                            headers=refresh_data_in)
        response = Response()
        response.data = as_refresh_response.json()
        return response


class VerifyJWTAuth(APIView):
    def post(self, request):
        verify_data_in = request.headers
        as_verify_response = requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_VERIFY}",
                                            headers=verify_data_in)
        response = Response()
        response.data = as_verify_response.json()
        return response


class PermissionsJWTAuth(APIView):
    def post(self, request):
        permission_data_in = json.loads(json.dumps(request.data))
        as_permission_response = requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_JWT_PERMISSIONS}",
                                             json=permission_data_in)
        response = Response()
        response.data = as_permission_response.json()
        return response


class AnalyticMain(APIView):
    permission_classes = [IsAnalytic]

    def get(self, request):
        # response = put_jwt_cookies()
        response = render(request, 'main_page_test.html', locals())
        show = request.COOKIES
        response.data = {'key': 'Key is valid', 'cookies': show}
        return response
        # return Response({'key': 'Key is valid', 'cookies': show})


class AnalyticUpload(APIView):
    permission_classes = [IsAnalytic]

    def get(self, request):
        return render(request, 'upload_file.html', locals())

    def post(self, request):
        file = request.FILES['file']
        uploading_file = UploadingTables({"file": file})
        if uploading_file:
            messages.success(request, 'Успешная загрузка')
        else:
            messages.error(request, 'Ошибка загрузки')
        return render(request, 'upload_file.html', locals())
