import requests
from config import *
from rest_framework import permissions
from rest_framework.exceptions import APIException
from monitoring import models


class IsAnalytic(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return requests.post(f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}/{AUTH_SERVICE_PATH_VERIFY}",
                             headers={
                                 'Authorization': f"Bearer {request.COOKIES['access_token_cookie']}"}).json().values()


class IsEngineer(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return models.Position.objects.filter(id_person=request.user.id, position="Engineer").count() > 0 or \
               models.Position.objects.filter(id_person=request.user.id, position="Admin").count() > 0



        # return (models.Position.objects.filter(id_person=request.user.id, position="Analytic").count() > 0
        #         or models.Position.objects.filter(id_person=request.user.id, position="Admin").count() > 0) \
        #         and requests.post(AUTH_VERIFY_ENDPOINT,
        #                          headers={'Authorization': f"Bearer {request.COOKIES['access_token_cookie']}"}).json().values()