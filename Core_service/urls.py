from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('monitoring/', include('monitoring.urls')),
    path('admin/', admin.site.urls),

    # path('api-auth/', include('rest_framework.urls')),
    # path('auth/', include('djoser.urls')),
    # path('auth/', include('djoser.urls.jwt')),

    #PATHS FOR AUTH SERVICE#
    # path('api/v1/login/', LoginJWTAuth.as_view(), name='django_login_jwt'),
    # path('api/v1/register/', RegisterJWTAuth.as_view(), name='django_register_jwt'),
    # path('api/v1/refresh/', RefreshJWTAuth.as_view(), name='django_refresh_jwt'),
    # path('api/v1/verify/', VerifyJWTAuth.as_view(), name='django_verify_jwt'),
    # path('api/v1/jwt-permissions/', PermissionsJWTAuth.as_view(), name='django_jwt_permissions'),
    #END_PATHS FOR AUTH SERVICE#
    # path('analytic/', AnalyticMain.as_view(), name='analytic_page'),
    # path('analytic/upload-file', AnalyticUpload.as_view(), name='upload_page')
    path("__debug__/", include("debug_toolbar.urls")),
]
