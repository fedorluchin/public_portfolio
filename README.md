# Система мониторинга промышленного оборудования

![PyPI - Version](https://img.shields.io/pypi/v/django?style=flat)
![PyPI - Python Version](https://img.shields.io/pypi/pyversions/django?style=flat)
![PyPI - License](https://img.shields.io/pypi/l/django)
![GitLab Stars](https://img.shields.io/gitlab/stars/fedorluchin%2Fpublic_portfolio)
![GitLab Forks](https://img.shields.io/gitlab/forks/fedorluchin%2Fpublic_portfolio)

**Система мониторинга промышленного оборудования** - это веб-приложение на языке Python, которое было создано для
удобства работы с технологическими установками, а также для взаимодействия в единой команде.
Система разработана на базе микросервисной архитектуры. Данный проект является пользовательским интерфейсом. 

## Внешние микросервисы
* [Микросервис для авторизации, аутентификации и регистрации.](https://gitlab.com/fedorluchin/public_portfolio/-/tree/Auth_Flask_MS?ref_type=heads)
* [Микросервис для симуляции отправки данных с датчиков](https://gitlab.com/fedorluchin/public_portfolio/-/tree/Fast_Api_Kafka?ref_type=heads)
## Структурная схема

![image](static/django_struct.drawio.png)

## Функционал
Распределение по должностям:
  - Аналитик (п. 1, 3)
  - Инженер (п. 2, 3)
  - Тестировщик (п. 1-3)

Каждая должность имеет доступ только к определенному функицоналу приложения.
Онлайн-чат доступен для всех авторизованных пользователей.


### 1. Отслеживание вольтажа установки
  - [x] Отображение данных в режиме реального времени на графике;
  - [x] Уведомление оператора о превышении порогового напряжения;
  - [x] Скачивание сводного файла с данными о вольтаже за выбранный период.
### 2. Взаимодействие с расписанием для распределения работников по установкам
  - [x] Отображение расписания, добавление/удаление записей из таблицы.
### 3. Онлайн чат для сотрудников
  - [x] Возможность создания комнат для общения между сотрудниками;
  - [x] Отображение подключенных к комнате позьзователей.

## Основные маршруты
/monitoring/login/ - авторизация пользователя в системе;

/monitoring/register/ - регистрация нового пользователя в системе;

/monitoring/home/ - домашняя страница приложения для выбора нужной функции.

## Установка
Клонируйте репозиторий и установите файл requirements.txt в среде Python>=3.9.0, включая Django >=4.0.
```
git clone https://gitlab.com/fedorluchin/public_portfolio.git -b Core_Django_MS
cd public_portfolio
pip install -r requirements.txt
```
## Пример использования
Запустите Django проект на локальном хосте, используя следующий код:
```
python .\manage.py makemigrations
python .\manage.py migrate
python .\manage.py runserver
```
Запустите внешний [микросервис на Flask](https://gitlab.com/fedorluchin/public_portfolio/-/tree/Auth_Flask_MS?ref_type=heads)
на локальном хосте (по умолчанию порт 5000) и [микросервис на FastApi](https://gitlab.com/fedorluchin/public_portfolio/-/tree/Fast_Api_Kafka?ref_type=heads)
(порт 8200).
```
python app.py 
```
Проект взаимодействует с базой данных [PostgreSQL](https://www.postgresql.org/download/), при необходимости измените
файл settings.py для настройки под себя.

## Тестирование
Для выполнения тестирования проекта, в пакете приложения **monitoring** находится пакет **tests**.
Запуск нужного теста выполняется с помощью команды:
```
python manage.py test monitoring.tests.test_file_name
```

## Docker
Проект с базой данных может быть развернут с помощью Docker. Для сборки используйте следующие команды:
```docker
docker-compose build
docker-compose up
docker exec -it container_name python ./manage.py createsuperuser
```


