FROM python:3.9

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/dj_ms

COPY ./requirements-docker.txt /usr/src/requirements-docker.txt
#RUN pip install --upgrade pip
RUN pip install -r /usr/src/requirements-docker.txt

COPY . /usr/src/dj_ms

#EXPOSE 8000
#CMD ["python", "manage.py", "migrate"]
#CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]