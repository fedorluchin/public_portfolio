AUTH_SERVICE_HOST = 'http://127.0.0.1' # for docker change param to the true ip http://{ip docker service}
AUTH_SERVICE_PORT = '5000'
AUTH_SERVICE_PATH_LOGIN = 'api/v1/login'
AUTH_SERVICE_PATH_REGISTER = 'api/v1/register'
AUTH_SERVICE_PATH_REFRESH = 'api/v1/refresh'
AUTH_SERVICE_PATH_VERIFY = 'api/v1/verify'
AUTH_SERVICE_PATH_JWT_PERMISSIONS = 'api/v1/jwt-permissions'

KAFKA_HOST = 'localhost'
KAFKA_PORT = '9092'
KAFKA_TOPIC_NAME_1 = 'voltage_1'

ACCESS_TOKEN_COOKIE_NAME = "access_token_cookie"
REFRESH_TOKEN_COOKIE_NAME = "refresh_token_cookie"

ANALYTIC_PERMISSION_NAME = "Analytic"
ENGEENER_PERMISSION_NAME = "Engineer"
TESTER_PERMISSION_NAME = "Tester"

