const socket = new WebSocket('ws://localhost:8000/ws/graph/');

const alertThreshold = 220; // Пороговое значение для алерта

const ctx = document.getElementById('myChart').getContext('2d');

// Рисуем горизонтальную линию по пороговому значению alertThreshold
Chart.register({
    id: 'horizontalLine',
    afterDraw: (chart, args, options) => {
        const ctx = chart.ctx;
        const yValue = chart.scales.y.getPixelForValue(alertThreshold);

        if (yValue) {
            ctx.save();
            ctx.beginPath();
            ctx.moveTo(chart.chartArea.left, yValue);
            ctx.lineTo(chart.chartArea.right, yValue);
            ctx.lineWidth = 2;
            ctx.strokeStyle = 'red';
            ctx.stroke();
            ctx.restore();
        }
    }
});

const myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Voltage',
            data: [],
            borderWidth: 1,
            borderColor: 'black' // Цвет линии графика
        }]
    },
    options: {
        plugins: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'График напряжения с датчика (V(В)/t(с))',
                font: {
                    size: 20 // Увеличение размера текста заголовка
                }
            }
        },
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});


const alertElement = document.createElement('div');
alertElement.style.position = 'absolute';
alertElement.style.top = '10px';
alertElement.style.right = '200px';
alertElement.style.background = 'red';
alertElement.style.color = 'white';
alertElement.style.padding = '10px';
alertElement.style.display = 'none'; // Начально скрываем алерт

document.body.appendChild(alertElement);

socket.onmessage = function(event) {
    const data = JSON.parse(event.data);

    // Проверка, есть ли уже такое значение с датой и временем на графике
    if (!myChart.data.labels.includes(data.dt_value)) {
        myChart.data.labels.push(data.dt_value);
        myChart.data.datasets[0].data.push(data.voltage);
    }

    // Добавление алерта
    if (data.voltage > alertThreshold) {
        // myChart.data.datasets[0].borderColor = 'red'; // Изменение цвета линии на красный
        alertElement.innerText = 'Превышение напряжения!';
        alertElement.style.display = 'block'; // Показываем алерт
    } else {
        alertElement.style.display = 'none'; // Скрываем алерт, если значение в норме
    }

    myChart.update();
};